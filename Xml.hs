module Xml where

--import Debug.Trace
import Parser

data XmlTree = MkXmlTree [XmlPrologueLine] XmlElement

data XmlAttribut = MkXmlAttribut String String

type XmlNom = String

data XmlElement = MkXmlElement XmlNom [XmlAttribut] [TextOrElement]

data TextOrElement =
      TextElement String
    | Child XmlElement

data XmlPrologueLine = MkXmlPrologueLine XmlNom [XmlAttribut]

exampleXmlTree =
  MkXmlTree
  []
  (MkXmlElement "ma-balise" [MkXmlAttribut "src" "example.org"]
   [TextElement "foo",
    Child (MkXmlElement "b" []
           [TextElement "texte en gras"]
          ),
    TextElement "bar"
   ]
  )

parseXmlWithErrors :: String -> Either String XmlTree
parseXmlWithErrors str =
  case parseXmlTree str of
  Left remaining -> Left ("Parsing failed, this is what remained: " ++ remaining)
  Right xmlTree -> Right xmlTree


parseXmlTree :: String -> Either String XmlTree
parseXmlTree str =
  case parse xmlTreeParser str of
  Left remaining -> Left remaining
  Right (xmlTree, _) -> Right xmlTree


xmlTreeParser :: Parser XmlTree
xmlTreeParser =
  do rootElem <- xmlElement
     zeroOrMoreWhiteSpace
     endOfString -- nothing should remain
     return (MkXmlTree [] rootElem)

xmlElement :: Parser XmlElement
xmlElement =
  do (name,attributs) <- openningTag
     childs <- zeroOuPlus textOrNestedElement
     closingTag name
     return (MkXmlElement name attributs childs)

openningTag :: Parser (String, [XmlAttribut])
openningTag = do lessThanSign
                 name <- elementName
                 attrs <- zeroOuPlus attribut
                 greaterThanSign
                 return (name, attrs)

-- TODO refactor name of this function to a generic name (including element name and attribute name)
elementName :: Parser String
elementName = do first <- validElementNameFirstChar
                 remaining <- zeroOuPlus validElementNameOtherChar
                 return (first:remaining)

--TODO only exclude the quote type used to open the value. Example : If value="" then ' not allowed. If value='' then " not allowed
--TODO check unicity of attributes
attribut :: Parser XmlAttribut
attribut = do oneOrMoreWhiteSpace
              attrName <- elementName
              zeroOrMoreWhiteSpace
              equal
              zeroOrMoreWhiteSpace
              quotes
              value <- zeroOuPlus validAttributeValueChar
              quotes
              return (MkXmlAttribut attrName value)

textOrNestedElement :: Parser TextOrElement
textOrNestedElement = nestedElement ||| textElement

nestedElement :: Parser TextOrElement
nestedElement = do child <- xmlElement
                   return (Child child)

textElement :: Parser TextOrElement
textElement = do text <- unOuPlus validTextElementChar
                 return (TextElement text)

closingTag :: String -> Parser Char
closingTag name = do lessThanSign
                     slash
                     parserWord name
                     greaterThanSign

------------------------------------------------------------

lessThanSign = parserChars ['<']
greaterThanSign = parserChars ['>']
slash = parserChars ['/']
equal = parserChars ['=']
quotes = parserChars "\"'"

------------------------------------------------------------

validAttributeValueChar =
  parserAnyCharExcept "<\"'"

validTextElementChar =
  parserAnyCharExcept "<"

validElementNameFirstChar =
  parserAnyCharExcept (".-0123456789" ++ elementNameCharsBlacklist)

validElementNameOtherChar =
  parserAnyCharExcept elementNameCharsBlacklist

elementNameCharsBlacklist = " !\"#$%&'()*+,/;<=>?@[\\]^`{|}~"

------------------------------------------------------------

zeroOrMoreWhiteSpace = zeroOuPlus (parserChars " \n\t")
oneOrMoreWhiteSpace = unOuPlus (parserChars "\n \t")

------------------------------------------------------------

showXmlTree :: XmlTree -> String
showXmlTree (MkXmlTree prologLines rootElem) =
  showXmlElement rootElem

showXmlElement :: XmlElement -> String
showXmlElement (MkXmlElement name attributs textsOrElements) =
  "<" ++ name ++ showXmlAttributs attributs ++  ">" ++
  showTextsOrElements textsOrElements  ++
  "</" ++ name ++ ">"

showXmlAttributs :: [XmlAttribut] -> String
showXmlAttributs attrs =
  concatMap (\attr -> " " ++ showXmlAttribut attr) attrs

showTextsOrElements :: [TextOrElement] -> String
showTextsOrElements textsOrElems =
  concatMap (\textOrElem -> showTextsOrElement textOrElem) textsOrElems

showTextsOrElement :: TextOrElement -> String
showTextsOrElement (TextElement str) =
  str
showTextsOrElement (Child element) =
  showXmlElement element

showXmlAttribut :: XmlAttribut -> String
showXmlAttribut (MkXmlAttribut key value) =
  key ++ "=\"" ++ value ++ "\""

------------------------------------------------------------
