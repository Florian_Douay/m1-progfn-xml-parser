module ParserHelpers
    where

    removePrefix :: String -> String -> Maybe String
    removePrefix [] strWithoutPrefix = Just strWithoutPrefix
    removePrefix _ [] = Nothing
    removePrefix (p:refix) (s:trToMatch) =
                      if p == s then
                        removePrefix refix trToMatch
                      else
                        Nothing
