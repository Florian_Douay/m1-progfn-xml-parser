module Test where

import Test.HUnit
import XmlTest hiding (main)
import ParserTest hiding (main)


allTests = test [ParserTest.tests,
                 XmlTest.tests]

main = do runTestTT allTests
