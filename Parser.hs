module Parser(
              Parser,
              caractere,
              parse,
              (|||),
              parserChars,
              endOfString,
              parserWord,
              parserAnyCharExcept,
              unOuPlus,
              zeroOuPlus,
             )
    where
    --import Debug.Trace
    import ParserHelpers

    -- GHC >= 7.10
    import Control.Monad       (liftM, ap)

    -- By convention, Right is a positive result, Left is a failure
    -- it will contain the remaining string to parse. When a parser returns
    -- Left, we choose to keep all the string that was *passed* to the function.
    type Resultat a = Either String (a, String)
    newtype Parser a = MkParser (String -> Resultat a)

    echoue :: Parser a
    echoue = MkParser (\_ -> Left "")

    retourne :: a -> Parser a
    retourne v = MkParser (\s -> Right (v, s))

    caractere :: Parser Char
    caractere = MkParser (\s -> case s of
                              ""     -> Left s
                              (c:cs) -> Right (c, cs))

    parse :: Parser a -> String -> Resultat a
    parse (MkParser p) = p

    endOfString :: Parser Char -- check that string is empty
    endOfString = MkParser (\str -> case str of
                              "" -> Right ('ε',"")
                              _ -> Left str)

    parserChars list = MkParser (parseChars list)
    parseChars _ "" = Left ""
    parseChars list (x:xs) =
      if x `elem` list
      then Right (x,xs)
      else Left (x:xs)

    -- find a way to merge with parserChars that doesn't reduce readability
    parserAnyCharExcept :: [Char] -> Parser Char
    parserAnyCharExcept blacklist = MkParser (parseAnyCharExcept blacklist)
    parseAnyCharExcept blacklist (x:xs) =
      if x `notElem` blacklist
      then Right (x,xs)
      else Left(x:xs)

    parserWord :: String -> Parser String
    parserWord word =
      MkParser (\str -> case removePrefix word str of
                  Just strWithoutPrefix -> Right (word, strWithoutPrefix)
                  Nothing -> Left str)


    (|||) :: Parser a -> Parser a -> Parser a
    p ||| p' = MkParser (\s -> case parse p s of
                                 Left _ -> parse p' s
                                 r       -> r)

    (>>>) :: Parser a -> (a -> Parser b) -> Parser b
    p >>> pf = MkParser (\s -> case parse p s of
                                 Left str   -> Left str
                                 Right (a, s') -> parse (pf a) s')

    unOuPlus :: Parser a -> Parser [a]
    unOuPlus p =
        (
            do
                x <- p
                xs <- zeroOuPlus p
                return (x:xs)
        )
        |||
        ( do x <- p ; return [x] )

    zeroOuPlus :: Parser a -> Parser [a]
    zeroOuPlus  p =
        unOuPlus p
        |||
        return []



    -- Stuff to make Parser a Monad (and use the do notation).
    -- Stop reading here for your sanity.
    -- GHC >= 7.10 https://wiki.haskell.org/Functor-Applicative-Monad_Proposal
    instance Functor Parser where
        fmap = liftM
    instance Applicative Parser where
        pure  = retourne {- move the definition of `return` from the `Monad` instance here -}
        (<*>) = ap
    instance Monad Parser where
        return = pure -- redundant since GHC 7.10 due to default impl
        (>>=) = (>>>)
        fail _ = echoue

    {--
    --  ghc < 7.10
    instance Monad Parser where
        (>>=) = (>>>)
        return = retourne
        fail _ = echoue
    --}
