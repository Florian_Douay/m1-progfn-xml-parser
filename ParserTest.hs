module ParserTest where

import Test.HUnit
import Parser

parserWordTest =
  TestCase (assertEqual "too lazy to write a message"
            (Right ("foo", "bar"))
            (parse (parserWord "foo") "foobar")
           )


parserWordFailTest =
  TestCase (assertEqual "too lazy to write a message"
            (Left "foobar")
            (parse (parserWord "bar") "foobar")
           )

parserWordFailTest2 =
  TestCase (assertEqual "too lazy to write a message"
            (Left "")
            (parse (parserWord "bar") "")
           )

parserAnyCharExceptTest =
  TestCase (assertEqual "too lazy to write a message"
            (Right ('f', "oo"))
            (parse (parserAnyCharExcept "bar") "foo")
           )

parserAnyCharExceptFailTest =
  TestCase (assertEqual "too lazy to write a message"
            (Left "foo")
            (parse (parserAnyCharExcept "barf") "foo")
           )

parserAnyCharExceptLessThan =
  TestCase (assertEqual "too lazy to write a message"
            (Left "</balise>")
            (parse (parserAnyCharExcept "<") "</balise>")
           )


parserZeroOuPlusOfAnyCharExcept =
  TestCase (assertEqual "too lazy to write a message"
            (Right ("content", "</balise>"))
            (parse (zeroOuPlus(parserAnyCharExcept "<")) "content</balise>")
           )

-- TODO ask: It can't work, ok with that. But why an infinite loop?
parserZeroOuPlusOfZeroOuPlusOfAnyCharExcept =
  TestCase (assertEqual "too lazy to write a message"
            (Right (["content"], "</balise>"))
            (parse (zeroOuPlus(zeroOuPlus (parserAnyCharExcept "<"))) "content</balise>")
           )



tests = TestList [parserWordTest,
                  parserWordFailTest,
                  parserWordFailTest2,
                  parserAnyCharExceptTest,
                  parserAnyCharExceptFailTest,
                  parserAnyCharExceptLessThan,
                  parserZeroOuPlusOfAnyCharExcept
                 ]

main = runTestTT tests
