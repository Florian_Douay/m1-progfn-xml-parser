module XmlTest where

import Test.HUnit
import Xml


xmlTree :: XmlTree
xmlTree =
  MkXmlTree
  []
  (MkXmlElement "ma-balise" [MkXmlAttribut "src" "example.org"]
   [TextElement "foo",
    Child (MkXmlElement "b" []
           [TextElement "texte en gras"]
          ),
    TextElement "bar"
   ]
  )


showXmlTreeTest =
  TestCase (assertEqual "showXmlTree failed"
            "<ma-balise src=\"example.org\">foo<b>texte en gras</b>bar</ma-balise>"
            (showXmlTree xmlTree))

showExampleTreeTest =
  TestCase (assertEqual "show exampleTree (in Xml.hs failed)"
            "<ma-balise src=\"example.org\">foo<b>texte en gras</b>bar</ma-balise>"
            (showXmlTree exampleXmlTree))

showMinimalTreeTest =
  TestCase (assertEqual "showMinimalTree failed"
            "<balise></balise>"
            (showXmlTree (MkXmlTree [] (MkXmlElement "balise" [] [])))
           )

parseMinimalTreeTest =
  TestCase (assertEqual "parseMinimalTree failed"
            "<balise></balise>"
            (showEitherXmlTree $ parseXmlTree "<balise></balise>")
           )

parseXmlShouldNotAllowNonMatchingElementTagsTest =
  TestCase (assertEqual "parseMinimalTree failed"
            "FAILED, remaining: 'othertag>'"
            (showEitherXmlTree $ parseXmlTree "<sometag></othertag>")
           )

parseXmlShouldNotAllowTwoRootsTest =
  TestCase (assertEqual "should not allow two roots"
            "FAILED, remaining: '<foo></foo>'"
            (showEitherXmlTree $ parseXmlTree "<foo></foo><foo></foo>")
           )

parseXmlShouldAllowAccentsAndDashesInTagNamesTest =
  TestCase (assertEqual "should allow '-' and 'à' in tag names"
            "<foo-bàr></foo-bàr>"
            (showEitherXmlTree $ parseXmlTree "<foo-bàr></foo-bàr>")
           )

parseXmlShouldNotAllowDashAtBeginningOfTagNamesTest =
  TestCase (assertEqual "should not allow '-' at beginning of tag names"
            "FAILED, remaining: '-bar></-bar>'"
            (showEitherXmlTree $ parseXmlTree "<-bar></-bar>")
           )
parseXmlShouldNotAllowDotAtBeginningOfTagNamesTest =
  TestCase (assertEqual "should not allow '.' at beginning of tag names"
            "FAILED, remaining: '.bar></.bar>'"
            (showEitherXmlTree $ parseXmlTree "<.bar></.bar>")
           )
parseXmlShouldNotAllowDigitsAtBeginningOfTagNamesTest =
  TestCase (assertEqual "should not allow '5' at beginning of tag names"
            "FAILED, remaining: '5bar></5bar>'"
            (showEitherXmlTree $ parseXmlTree "<5bar></5bar>")
           )

parseXmlShouldAllowTextInsideElementsTest =
  TestCase (assertEqual "should parse text inside element"
            "<bar>foo</bar>"
            (showEitherXmlTree $ parseXmlTree "<bar>foo</bar>")
           )

parseXmlShouldAllowNestedElementInsideElementsTest =
  TestCase (assertEqual "should parse element inside element"
            "<bar><foo></foo></bar>"
            (showEitherXmlTree $ parseXmlTree "<bar><foo></foo></bar>")
           )

parseXmlShouldAllowManyNestedAndTextElementsCombinationsTest1 =
  TestCase (assertEqual "should parse (text inside element) inside element"
            "<bar><foo>baz</foo></bar>"
            (showEitherXmlTree $ parseXmlTree "<bar><foo>baz</foo></bar>")
           )

parseXmlShouldAllowManyNestedAndTextElementsCombinationsTest2 =
  TestCase (assertEqual "should parse (text and element) inside element"
            "<bar>baz<foo></foo></bar>"
            (showEitherXmlTree $ parseXmlTree "<bar>baz<foo></foo></bar>")
           )

parseXmlShouldAllowManyNestedAndTextElementsCombinationsTest3 =
  TestCase (assertEqual "should parse (element and text) inside element"
            "<bar><foo></foo>baz</bar>"
            (showEitherXmlTree $ parseXmlTree "<bar><foo></foo>baz</bar>")
           )

parseXmlShouldNotAllowInvalidNestedAndTextElementsCombinationsTest1 =
  TestCase (assertEqual "should not parse overlapping tags. The containing tag must be closed after its children"
            "FAILED, remaining: 'bar></foo></bar>'" -- TODO find why the parser stops at the first "bar" it seems early
            (showEitherXmlTree $ parseXmlTree "<foo><bar></foo></bar>")
           )

parseXmlShouldNotAllowInvalidNestedAndTextElementsCombinationsTest2 =
  TestCase (assertEqual "should not parse overlapping tags. The containing tag must be closed after its children"
            "FAILED, remaining: 'bar></foo>baz</bar>'" -- TODO find why the parser stops at the first "bar" it seems early
            (showEitherXmlTree $ parseXmlTree "<foo><bar></foo>baz</bar>")
           )

parseXmlShouldHandleWhiteSpaces =
  TestCase (assertEqual "should drop whitespace at end of file at keep the rest"
            "<bar>\n\
             \  \t<foo>\n\
             \  \t</foo>\n\
             \</bar>"
            (showEitherXmlTree $ parseXmlTree "<bar>\n\
                                             \  \t<foo>\n\
                                             \  \t</foo>\n\
                                             \</bar> \n \t \n")
           )

parseXmlShouldAllowAnAttribute =
  TestCase (assertEqual "should allow an attribute"
            "<foo src=\"example.org\"></foo>"
            (showEitherXmlTree $ parseXmlTree "<foo src=\"example.org\"></foo>")
           )

parseXmlShouldAllowMultipleAttributes =
  TestCase (assertEqual "should allow multiple attributes"
            "<foo src=\"example.org\" alt=\"one basic example\" id=\"  10   \"> Paris </foo>"
            (showEitherXmlTree $ parseXmlTree "<foo src=\"example.org\" alt=\"one basic example\"  id  = \"  10   \"> Paris </foo>")
           )

parseXmlShouldNotAllowInvalidAttributeValue1 =
  TestCase (assertEqual "should not allow < in attribute value"
            "FAILED, remaining: ' src=\"<example.org\"></foo>'"
            (showEitherXmlTree $ parseXmlTree "<foo src=\"<example.org\"></foo>")
           )

parseXmlShouldNotAllowInvalidAttributeValue2 =
  TestCase (assertEqual "should not allow ' in attribute value"
            "FAILED, remaining: 'example.org'\"></foo>'"
            (showEitherXmlTree $ parseXmlTree "<foo src=\"'example.org'\"></foo>")
           )

parseXmlShouldNotAllowInvalidAttributeValue3 =
  TestCase (assertEqual "should not allow \" in attribute value"
            "FAILED, remaining: 'example.org'\"></foo>'"
            (showEitherXmlTree $ parseXmlTree "<foo src=\"\"example.org'\"></foo>")
           )

parseXmlShouldAllowUTF8 =
  TestCase (assertEqual "should allow UTF8"
            "<俄语 լեզու=\"ռուսերեն\">данные</俄语>"
            (showEitherXmlTree $ parseXmlTree "<俄语 լեզու=\"ռուսերեն\">данные</俄语>")
           )


parseXmlWithErrorsShouldReturnXmlTreeWhenValidXml =
  TestCase (assertEqual "should return tree"
            "<foo></foo>"
            (showEitherXmlTreeWithoutmodifingMessage $  parseXmlWithErrors "<foo></foo>")
           )

parseXmlWithErrorsShouldErrorMessageWhenInvalidXml =
 TestCase (assertEqual "should return error message"
           "Parsing failed, this is what remained: foo>"
           (showEitherXmlTreeWithoutmodifingMessage $ parseXmlWithErrors "<fail></foo>")
          )


{-
parseXmlShouldAllow =
  TestCase (assertEqual "should"
            ("<foo></foo>")
            (showEitherXmlTree $ parseXmlTree "<foo></foo>")
           )
-}



tests = TestList [showXmlTreeTest,
                  showExampleTreeTest,
                  showMinimalTreeTest,
                  parseMinimalTreeTest,
                  parseXmlShouldNotAllowNonMatchingElementTagsTest,
                  parseXmlShouldNotAllowTwoRootsTest,
                  parseXmlShouldAllowAccentsAndDashesInTagNamesTest,
                  parseXmlShouldNotAllowDashAtBeginningOfTagNamesTest,
                  parseXmlShouldNotAllowDotAtBeginningOfTagNamesTest,
                  parseXmlShouldNotAllowDigitsAtBeginningOfTagNamesTest,
                  parseXmlShouldAllowTextInsideElementsTest,
                  parseXmlShouldAllowNestedElementInsideElementsTest,
                  parseXmlShouldAllowManyNestedAndTextElementsCombinationsTest1,
                  parseXmlShouldAllowManyNestedAndTextElementsCombinationsTest2,
                  parseXmlShouldAllowManyNestedAndTextElementsCombinationsTest3,
                  parseXmlShouldNotAllowInvalidNestedAndTextElementsCombinationsTest1,
                  parseXmlShouldNotAllowInvalidNestedAndTextElementsCombinationsTest2,
                  parseXmlShouldHandleWhiteSpaces,
                  parseXmlShouldAllowAnAttribute,
                  parseXmlShouldAllowMultipleAttributes,
                  parseXmlShouldNotAllowInvalidAttributeValue1,
                  parseXmlShouldNotAllowInvalidAttributeValue2,
                  parseXmlShouldNotAllowInvalidAttributeValue3,
                  parseXmlShouldAllowUTF8,
                  parseXmlWithErrorsShouldReturnXmlTreeWhenValidXml,
                  parseXmlWithErrorsShouldErrorMessageWhenInvalidXml
--                  parseXmlShouldAllow
                 ]

showEitherXmlTree (Right xmlTree) = showXmlTree xmlTree
showEitherXmlTree (Left remainingToParser) = "FAILED, remaining: '" ++ remainingToParser ++ "'"
showEitherXmlTreeWithoutmodifingMessage (Right xmlTree) = showXmlTree xmlTree
showEitherXmlTreeWithoutmodifingMessage (Left remainingToParser) = remainingToParser

main = runTestTT tests
