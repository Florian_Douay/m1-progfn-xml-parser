import Parser

data ABin =
    Leaf
  | Node ABin ABin
  deriving (Show)

-- example:
main = print(parse parserABin "((f,(f,f)),f)")

-- TODO: add another layer of parser that ensure that the final string is empty
parserABin :: Parser ABin
parserABin = parserLeaf ||| parserNode


parserNode :: Parser ABin
parserNode = do parserOpenPar
                child1 <- parserABin
                parserComma
                child2 <- parserABin
                parserClosingPar
                return (Node child1 child2)

parserLeaf = do parserChars ['f']
                return Leaf
parserOpenPar = parserChars ['(']
parserClosingPar = parserChars [')']
parserComma = parserChars [',']
